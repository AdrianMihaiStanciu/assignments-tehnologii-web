
const FIRST_NAME = "Adrian-Mihai";
const LAST_NAME = "Stanciu";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
function initCaching() {
   var object = {about:0,home:0,contact:0};
   object.pageAccessCounter = function(a){
       if (a==undefined){
        object.home++;
    }      
        else{       
       if (a.toUpperCase()=='ABOUT'){
           object.about++;          
       }
       if (a.toUpperCase()=='CONTACT'){
        object.contact++;         
    }
        }
   }
   object.getCache = function(){
       return object;
   }
   return object;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

